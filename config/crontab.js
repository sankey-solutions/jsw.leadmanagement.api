var token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ5MzEyOTY3ODU2MSIsImlhdCI6MTUxMjk4OTIyM30.frhMW5hvQev4SloRS9SddYyhodiVeb_0DaIhx3tI-hCuWWt0dtcERSn8fJmoiYY3WWkByAUuVOVeEltAwWgwXQ";
var reqBody = {};
module.exports.crontab = {
    /*
     * The asterisks in the key are equivalent to the
     * schedule setting in crontab, i.e.
     * minute hour day month day-of-week year
     * so in the example below it will run every minute
     */

    '00 00 04 * * *': function() {
        require('../api/models/Leads').sendLeadReportAsAttachmentInMail(reqBody, token, function(response) {
            console.log("Completed");
        });
    },

};