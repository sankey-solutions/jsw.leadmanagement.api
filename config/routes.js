/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
   * etc. depending on your default view engine) your home page.              *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  //leads
  'post /addLead/v1': 'LeadsController.addLead',
  'post /getLeads/v1': 'LeadsController.getLeads',
  'post /updateLead/v1': 'LeadsController.updateLead',
  'post /getHallOfFame/v1': 'LeadsController.getHallOfFame',
  'post /countLeads/v1': 'LeadsController.countLeads',
  'post /getLeadReport/v1': 'LeadsController.getLeadReport',

  //Added by sushant  on 27/06/2018 for adding star of month
  'post /addStarOfMonth/v1': 'StarOfMonthController.addStarOfMonth',
  'post /getStarOfMonth/v1': 'StarOfMonthController.getStarOfMonth',


  'post /updateLeadsforCorrection/v1': 'LeadsController.updateLeadsforCorrection',
  'post /sendLeadReportAsAttachment/v1': 'LeadsController.sendLeadReportAsAttachment',


  // 'post /getLeads': 'LeadsController.getLeadsSummary',
  // 'get /detailleads': 'LeadsController.getLeads',
  // 'get /lead': 'LeadsController.getLead',
  // 'put /lead/:id': 'LeadsController.updateLead',
  // // 'post /lead': 'LeadsController.addLead',
  // 'delete /lead/:id': 'LeadsController.deleteLead',
  // 'get /visitreport': 'LeadsController.visitReport',
  // 'get /influencerreport': 'LeadsController.influencerReport',

  /***************************************************************************
   *                                                                          *
   * Custom routes here...                                                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the custom routes above, it   *
   * is matched against Sails route blueprints. See `config/blueprints.js`    *
   * for configuration options and examples.                                  *
   *                                                                          *
   ***************************************************************************/

};
