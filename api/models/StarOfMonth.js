/**
 * StarOfMonth.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 * created by sushant on 30-06-2018
 */
"use strict";
var request = require("request");
var async = require("async");

module.exports = {
	tableName: 'StarOfMonth',

	attributes: {


		engineerId: {
			type: 'string',
			// required: true
		},
		mobileNo: {
			type: 'string',
			//  required: true
		},
		userName: {
			type: 'string',
			//  required: true
		},
		city: {
			type: 'string'
		},
		state: {
			type: 'string',
			defaultsTo: 0
		},
		noOfLeads: {
			type: 'integer',
			defaultsTo: 0
		},
		month: {
			type: 'integer',
			//  required: true
		},
		year: {
			type: 'integer',
			// required: true
		},
		clientId: {
			type: 'string',
			// required: true
		},
		programId: {
			type: 'string',
			//  required: true
		},


		isDeleted: {
			type: 'boolean',
			defaultsTo: false
		}
	},

	addStarOfMonth: function (reqBody, programInfo, token, userLogObject, next) {
		console.log("StarOfMonth in model")
		// default response object
		var responseObj = {
			"statusCode": -1,
			"message": null,
			"result": null
		};
		//Variable defined to create dataLogObject
		var dataLogObject = {
			"userId": userLogObject.userId,
			"requestApi": userLogObject.requestApi,
			"eventType": userLogObject.eventType,
			"collection": "Add Secondary Sale",
		}

		var programId = reqBody.programId; // program id
		var clientId = reqBody.clientId;
		var month = reqBody.month;
		var year = reqBody.year;

		var starArray = [];		// to hold array of objects returned from excel to JSON
		var engineersArray = [];	// to hold engineers who are star of month
		var finalRecordsArray = []; // final array to be inserted


		// Request API Setup for calculatePointsForLeadManagement
    	//api to get all program users
		var apiURL = ServConfigService.getApplicationConfig().base_url +
			":" +
			ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.port +
			ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.url;

		//request object
		// var requestObject = { programId: programId, frontendUserInfo: reqBody.frontendUserInfo };
		// requestObject.pointsType = 	"starOfMonth";
		// requestObject.clientId = clientId;
		var requestObjectArray = [];

		//request options to fetch program user
		var requestOptions = {
			url: apiURL,
			method: ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.method,
			headers: {
				'authorization': 'Bearer ' + token
			},
			//json: requestObject
		};

		console.log("requestOptions ----",requestOptions);

		ExcelToJsonService.convertExcelToJson(reqBody, function (response) {
			if(response.statusCode === 0){
				starArray = response.result;
				// Check if month and year in UI is same as in excel
				if(starArray.length > 0){
					if(month != parseInt(starArray[0].month)){
						responseObj.message = "Month selected and month in excel is different. Please select correct month !!";
						return next(responseObj);
					}
					else if(year != parseInt(starArray[0].year)){
						responseObj.message = "Year selected and year in excel is different. Please select correct year !!";
						return next(responseObj);
					}
				}
				else{
					responseObj.message = "No record present in Excel Upload. Please check !!";
					return next(responseObj);
				}
				// Logic to get engineers(unique) with Star of month as yes
				var i = 0, j = 0;
				for(i= 0;i<starArray.length;i++){
					if(starArray[i]['engineerId'] && starArray[i]['StarOfMonth'] === "Yes"){
						if(!engineersArray.includes(starArray[i]['engineerId'])){
							engineersArray.push(starArray[i]['engineerId']);
						}
					}
				}
				// Logic to get count of leads and add respective fields
				i= 0;
				for(i= 0;i<engineersArray.length;i++){
					var tempObj = {};
					tempObj.engineerId = engineersArray[i];
					tempObj.noOfLeads = 0;
					tempObj.month = month;
					tempObj.year = year;
					tempObj.clientId = clientId;
					tempObj.programId = programId;
					tempObj.starOfMonthId = "SI" + Date.now() + i;

					for(j= 0;j<starArray.length;j++){
						if(starArray[j]['engineerId'] === engineersArray[i] ){
							tempObj.mobileNo = starArray[j]['Mobile No'];		// field not present in Excel
							tempObj.userName = starArray[j]['User Name'];
							tempObj.city = starArray[j]['City'];
							tempObj.state = starArray[j]['State'];
							tempObj.noOfLeads = tempObj.noOfLeads + 1;
						}
					}
					finalRecordsArray.push(tempObj);
				}
				console.log("finalRecordsArray",finalRecordsArray);

				StarOfMonth.findOne({month: month, year: year}).exec(function (err, record) {
					if (err) {
						responseObj.message = err;
						next(responseObj);
					} else if (record) {
						responseObj.statusCode = 2;
						responseObj.message = "Star of month already uploaded for this month !!";
						next(responseObj);
					} else {
						StarOfMonth.create(finalRecordsArray).exec(function (err, records) {
							console.log("error", err);
							if (err) { // handling for error
								console.log("error  in creating record");
								responseObj.statusCode = -1;
								responseObj.message = err;
								sails.log.error("StarOfMonth-Model>addStarOfMonth>StarOfMonth.create", "Input:" + starArray, " error:" + err);
							} else if (!records) { // handling in case undefined/no object was returned
								responseObj.message = "undefined object was returned";
								sails.log.error("StarOfMonth-Model>addStarOfMonth>StarOfMonth.create", "Input:" + starArray, " result:" + records);
							}
							else { // record created successfully  
								responseObj.statusCode = 0;
								responseObj.message = "StarOfMonth record successfully created";
								responseObj.result = records;
								// LogService.addDataLog(dataLogObject, token);
								sails.log.info("StarOfMonth-Model>addStarOfMonth>StarOfMonth.create", responseObj.message);

								// TO DO : Call CalculateLeadPoints
								async.eachSeries(records, function(recordsObject, asyncEachCB) {

									var tempObj = {};
									tempObj.clientId = clientId;
									tempObj.programId = programId;
									tempObj.pointsType = "starOfMonth";
									tempObj.transactionId = recordsObject.starOfMonthId;
									tempObj.partyId = recordsObject.engineerId;
									tempObj.searchByMobile = recordsObject.mobileNo;
									tempObj.frontendUserInfo = reqBody.frontendUserInfo;
									// requestObjectArray.push(tempObj);
									requestOptions.json = tempObj;
									console.log("async each req option",requestOptions);
									request(requestOptions, function(error, response, body) {
										console.log("body from CalculateLeadPoints",body);
										asyncEachCB();
									});
									// asyncEachCB();
								}, function(err) {
								   console.log("in async each star of the months fun err");
								})
							}
							next(responseObj);
						});
						
					}
				});
			}
			else{
				responseObj = response;
				next(responseObj);
			}
		});
	},

	getStarOfMonth: function (reqBody, token, next) {
		//console.log("comeing here");
		var responseObj = {
			"statusCode": -1,
			"message": null,
			"result": null
		};
		var programId = reqBody.programId;
		//console.log("************************records are", record);
		StarOfMonth.find({programId:programId}).sort({year:-1,month:-1}).limit(1).exec(function (err, record) {
			if (err) {
				responseObj.message = err;
				next(responseObj);
			} else if (!record) {
				responseObj.message = "undefined object was returned";
				next(responseObj);
			} else if (record.length > 0) {
				responseObj.statusCode = 0;

				// Fetch all records of latest year and month
 				StarOfMonth.find({programId:programId, year:record[0].year, month:record[0].month}).sort({noOfLeads:-1}).exec(function (err, record) {
					if (err) {
						responseObj.message = err;
						next(responseObj);
					} else if (!record) {
						responseObj.message = "undefined object was returned";
						next(responseObj);
					} else if (record.length > 0) {
						responseObj.statusCode = 0;
						responseObj.message = "Star Of Month fetched successfully";
						responseObj.result = record;
						next(responseObj);
					} else {
						responseObj.statusCode = 2;
						responseObj.message = "Records not found";
						next(responseObj);
					}
				});

			} else {
				responseObj.statusCode = 2;
				responseObj.message = "Records not found";
				next(responseObj);
			}
		});
	},

};
