/**
 * Leads.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var request = require('request');
var json2xls = require('json2xls');
var fs = require('fs');

module.exports = {

    tableName: 'leads',

    attributes: {

        projectname: {
            type: 'string',
            required: true
        },
        ownername: {
            type: 'string',
            required: true
        },
        projecttype: {
            type: 'string'
        },
        expectedtonnage: {
            type: 'float' ,
            defaultsTo: 0
        },
        pincode: {
            type: 'integer',
            defaultsTo: 0
        },
        state: {
            type: 'string',
            required: true
        },
        city: {
            type: 'string',
            required: true
        },
        // keycontacts: [{
        //     name: {
        //         type: 'string'
        //     },
        //     role: {
        //         type: 'string'
        //     },
        //     contact: {
        //         type: 'string'
        //     }
        // }],
        keycontacts: {
            type: 'array'
        },
        influencers: [{
            iid: {
                type: 'string'
            },
            weight: {
                type: 'integer',
                defaultsTo: 0
            }
        }],
        createdby: {
            name: {
                type: 'string'
            },
            contact: {
                type: 'string'
            },
            role: {
                type: 'string'
            }
        },
        createdate: {
            type: 'date'
        },
        inputmedium: {
            type: 'string'
        },
        assignedto: {
            assignid: {
                type: 'integer'
            },
            name: {
                type: 'string'
            }
        },
        status: {
            type: 'string',
            required: true
        },
        estimation: [{
            sku: {
                type: 'string'
            },
            quantity: {
                type: 'integer'
            },
            unit: {
                type: 'string'
            },
            dealer: {
                id: {
                    type: 'integer'
                },
                name: {
                    type: 'string'
                }
            }
        }],
        visit: [{
            purpose: {
                type: 'string'
            },
            visitdate: {
                type: 'date'
            },
            time: {
                type: 'date'
            },
            mode: {
                type: 'string'
            },
            contacts: [{
                name: {
                    type: 'string'
                },
                role: {
                    type: 'string'
                },
                contact: {
                    type: 'string'
                }
            }],
            team: [{
                name: {
                    type: 'string'
                },
                role: {
                    type: 'string'
                },
                contact: {
                    type: 'string'
                }
            }],
            info: {
                type: 'string'
            }
        }],
        history: [{
            modifiedby: {
                type: 'string'
            },
            modifystatus: {
                type: 'string'
            },
            modifieddate: {
                type: 'date'
            }
        }],
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        }
    },


    //   # Method: addLead
    // # Description: to add a Lead

    addLead: function(reqBody, programInfo, token, userLogObject, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId": userLogObject.userId,
            "requestApi": userLogObject.requestApi,
            "eventType": userLogObject.eventType,
            "collection": "Add Secondary Sale",
        }

        var recordInfo = reqBody;
        var leadId = "LI" + Date.now();
        recordInfo.leadId = leadId;

        console.log("recordInfo", recordInfo);
        // record to be inserted
        // var programId = reqBody.programId; // program id
        // var clientId = reqBody.clientId;

        // creating the record
        Leads.create(recordInfo).exec(function(err, record) {
            console.log("record", record, "error", err);
            if (err) { // handling for error
                responseObj.message = err;
                sails.log.error("Leads-Model>addLead>Leads.create", "Input:" + recordInfo, " error:" + err);
                // callback("error in function 2", "function 2 failed");
            } else if (!record) { // handling in case undefined/no object was returned
                responseObj.message = "undefined object was returned";
                sails.log.error("Leads-Model>addLead>Leads.create", "Input:" + recordInfo, " result:" + record);
                // callback("error in function 2", "function 2 failed");
            } else { // record created successfully  
                responseObj.statusCode = 0;
                responseObj.message = "Leads record successfully created";
                responseObj.result = record;
                dataLogObject.newValue = record;
                Leads.getAllParents(record, userLogObject, token, function(response){

                });
                // LogService.addDataLog(dataLogObject, token);
                sails.log.info("Leads-Model>addLead>Leads.create", responseObj.message);
            }
            next(responseObj);
        }); // end of SecondarySales.create
    },

    // To get parent users
    getAllParents: function(reqBody, userLogObject, token, next) {
        console.log("111", reqBody);
        var userId = reqBody.providedby;
        var userType = reqBody.providedbyRole;
        var clientId = reqBody.clientId;
        var programId = reqBody.programId;
        var leadId = reqBody.leadId;

        var responseObj = {};
        var parentUsers = [];

        /********************************   REQUEST OBJECT FOR EXTERNAL API CALLS   ******************************/
        //Request api url and request object for Client User
        var apiUrl1 = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getAllClientUsers.port +
            ServConfigService.getApplicationAPIs().getAllClientUsers.url;

        var requestObject1 = {
            clientId: clientId,
            serviceType: "client",
            frontendUserInfo: userLogObject
        }

        var requestOptions1 = {
            url: apiUrl1,
            method: ServConfigService.getApplicationAPIs().getAllClientUsers.method,
            headers: {
                'authorization': 'Bearer ' +
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDMwNTQyOH0.oVHd01FMVlR56c-4Maiw5TTYNO8yZNND1Iyw2w3hm0DqoAyGNtMXanGuTy_yPZzmVRJUWQgIbYpNVfO_cYOpdw"
            },
            json: requestObject1
        };

        //Request api url and request object for BE User
        var apiURL2 = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.port +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.url;

        var requestObject2 = {
            clientId: clientId,
            programId: programId,
            serviceType: "programSetup",
            frontendUserInfo: userLogObject
        }

        var requestOptions2 = {
            url: apiURL2,
            method: ServConfigService.getApplicationAPIs().getAllProgramUsers.method,
            headers: {
                'authorization': 'Bearer ' +
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDMwNTQyOH0.oVHd01FMVlR56c-4Maiw5TTYNO8yZNND1Iyw2w3hm0DqoAyGNtMXanGuTy_yPZzmVRJUWQgIbYpNVfO_cYOpdw"
            },
            json: requestObject2
        };
        /****************************************************************************************************** */

        var requestOptions;
        var tempObj = {};


        var getParentUser = function(singleObject) {
            //console.log("111111111111111111111111111111111111111111111", singleObject.providedbyRole);
            //console.log("111111111111111111111111111111111111111111111", singleObject.providedby);

            if (singleObject.providedbyRole === "ClientUser") {
                requestOptions = requestOptions1;
                requestObject1.clientUserId = singleObject.providedby;
            } else if (singleObject.providedbyRole === "ChannelPartner") {
                requestOptions = requestOptions2;
                requestObject2.programUserId = singleObject.providedby;
            }
            //console.log("333", requestOptions);
            request(requestOptions, function(error, response, body) {
                
                if (error || body === undefined) { // error occured
              //      console.log("444","555", error,leadId);
                    responseObj.message = error;
                    next(responseObj);
                } else {
                    if (body.statusCode === 0) { // program found succesfully
                        responseObj.message = "Parent found";
                        if (singleObject.providedbyRole === 'ClientUser') {
                            //console.log("22222222222222222     ParentUsersInfo------",body.result.parentUserId);

                            if (body.result.parentUserId === 'None' || body.result.parentUserId === undefined) {
                                responseObj.result = parentUsers;
                                //console.log("333333333333333333333", parentUsers);
                                updateLead();
                            } else {
                                parentUsers.push(body.result.parentUserId);
                                tempObj.providedby = body.result.parentUserId;
                                tempObj.providedbyRole = "ClientUser";
                                //console.log("777", parentUsers);
                                getParentUser(tempObj);
                            }
                        } else {
                            // if user is mapped to parent; perform recursion
                            if (body.result[0]) {
                                if (body.result[0].parentUsersInfo) {
                                    //console.log("222222222222222    ParentUsersInfo------",body.result[0].parentUsersInfo[0].programUserId);
                                    parentUsers.push(body.result[0].parentUsersInfo[0].programUserId);
                                    tempObj.providedby = body.result[0].parentUsersInfo[0].programUserId;
                                    tempObj.providedbyRole = body.result[0].parentUsersInfo[0].userType;
                                    //console.log("888", parentUsers);
                                    getParentUser(tempObj);
                                } else {
                                    //console.log("4444444444444444444444", parentUsers);
                                    updateLead();
                                }
                            } else {
                                updateLead();
                            }
                        }
                    } else if (body.statusCode === 2) {
                        responseObj.statusCode = 2;

                        //SHOULD ADD DEBIT PARTY AS PARENT TO CREDIT PARTY
                        updateLead();
                    } else {
                        responseObj = body;
                        updateLead();
                    }

                }
            });
        }

        var updateLead = function() {
            // console.log("555555555555555555  parentUsers", parentUsers);
            // console.log("555555555555555555  LeadId", leadId);

            Leads.update({
                leadId: leadId
            }, {
                parentUsers: parentUsers
            }).exec(function(err, records) {
                if (err) {
                    responseObj.message = err;
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " error:" + err);
                    next(responseObj);
                } else if (!records) { // handling in case undefined/no object was returned
                    responseObj.message = "undefined object was returned";
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " result:" + records);
                    next(responseObj);
                } else if (records.length === 0) { // handling in case records was not updated
                    responseObj.message = "Record not found!!";
                    sails.log.error("Leads-Model>updateLeads>Leads.update ", " result:" + records);
                    next(responseObj);
                } else { // record successfully updated
                    responseObj.statusCode = 0;
                    responseObj.result = records[0];
                    // console.log("UPDATED UPDATED UPDATED");
                    sails.log.info("Leads-Model>updateLeads>Leads.update : Leads record updated successfully!!");
                    next(responseObj);
                }
            });
        }

        getParentUser(reqBody);
    },


    /*
     **  methodName: getLeads
     **  Description: to get Leads.
     */
    getLeads: function(requ, token, next) {
        console.log("request", requ);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var skip = requ.skip;
        var limit = requ.limit;
        var sort = requ.sort;
        var queryString = [{
            $match: {
                isDeleted: false
            }
        }];
        var countQueryString = {
            isDeleted: false
        };
        // var countQueryString;

        // if (requ.providedby !== undefined) { //filter using providedby
        //     countQueryString.providedby = requ.providedby;
        // }

        // if (requ.leadId !== undefined) { //filter using leadId
        //     countQueryString.leadId = requ.leadId;
        // }

        if (requ.providedby) {

            queryString.push({
                $match: {
                    // isDeleted: false,
                    "$or": [{
                            "providedby": requ.providedby
                        },
                        {
                            "parentUsers": {
                                "$in": [requ.providedby]
                            }
                        }

                    ],
                    "programId": requ.programId
                },
            });
            // countQueryString.isDeleted = false;
            countQueryString.programId = requ.programId;

            countQueryString.$or = [{
                    "providedby": requ.providedby
                },
                {
                    "parentUsers": {
                        "$in": [requ.providedby]
                    }
                }
            ];
            // if (requ.providedby !== undefined) { //filter using providedby
            // countQueryString.providedby = requ.providedby;
            //  }
        } else {
            queryString.push({
                $match: {
                    "programId": requ.programId
                },
            });
            countQueryString.programId = requ.programId;
        }

        if (requ.isQualified && requ.isPending) {
            queryString.push({
                $match: {
                    $or: [{
                        "qualified": requ.isQualified
                    }, {
                        "pending": requ.isPending
                    }]
                },
            });

            countQueryString.$or = [{
                    "qualified": requ.isQualified
                },
                {
                    "pending": requ.isPending
                }
            ]
        }

        if (requ.startDate && requ.endDate) {
            var startDate = new Date(requ.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            var endDate = new Date(requ.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            // startDate.toISOString();
            // var endDate = new Date(requ.endDate);
            // endDate.toISOString();
            queryString.push({
                $match: {
                    "createdAt": {
                        // $gte: new Date(requ.startDate),
                        $gte: startDate,
                        $lt: endDate
                    }
                },
            });
            countQueryString.createdAt = {
                $gte: new Date(startDate),
                $lt: new Date(endDate)
            }
        }

        if (requ.startDate !== undefined && requ.endDate === undefined) { //filter using startDate
            var startDate = new Date(requ.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            queryString.push({
                $match: {
                    "createdAt": {
                        $gte: new Date(startDate)
                    }
                },
            });
            countQueryString.createdAt = {
                $gte: new Date(startDate)
            }
        }

        if (requ.endDate !== undefined && requ.startDate === undefined) { //filter using endDate
            var endDate = new Date(requ.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            queryString.push({
                $match: {
                    "createdAt": {
                        $lt: new Date(endDate)
                    }
                },
            });
            countQueryString.createdAt = {
                $lt: new Date(endDate)
            }

        }



        if (requ.qualified) {
            queryString.push({
                $match: {
                    "qualified": requ.qualified
                },
            });
            countQueryString.qualified = requ.qualified
        }
        if (requ.rejected) {
            queryString.push({
                $match: {
                    "rejected": requ.rejected
                },
            });
            countQueryString.rejected = requ.rejected;
        }
        if (requ.pending) {
            queryString.push({
                $match: {
                    "pending": requ.pending
                },
            });
            countQueryString.pending = requ.pending;
        }

        if (sort !== undefined) {
            queryString.push({
                $sort: sort
            });

        }
        queryString.push({
            $group: {
                "_id": null,
                data: {
                    $push: "$$ROOT"
                }
            }
        }, {
            $unwind: "$data"
        })

        queryString.push({
            $lookup: {
                "from": "ProgramUsers",
                "localField": "data.providedby",
                "foreignField": "programUserId",
                "as": "programUserInfo"
            }
        })
        queryString.push({
            "$project": {
                "userName": "$programUserInfo.userDetails.userName",
                "data": 1
            }
        })
        if (requ.leadId) {
            queryString.push({
                $match: {
                    "leadId": requ.leadId
                },
            });
            countQueryString.leadId = requ.leadId
        }

        if (requ.status) {
            queryString.push({
                $match: {
                    "status": requ.status
                },
            });
            countQueryString.status = requ.status
        }


        if (skip !== undefined) {
            queryString.push({
                $skip: skip
            });

        }
        if (limit !== undefined) {
            queryString.push({
                $limit: limit
            });
        }

        Leads.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    responseObj.queryString = queryString;
                    console.log("records,err", records, err);
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Leads fetched successfully";
                        responseObj.result = records;
                        responseObj.countQueryString = countQueryString;

                        CommonOperations.findCount("Leads", countQueryString, token, function(response) {
                            console.log("Response******************************8", response);
                            if (response.statusCode === 0) {
                                responseObj.statusCode = 0;
                                // responseObj.message = "Count Fetched"
                                // responseObj.result = responseObj.count;
                                responseObj.countQueryString = countQueryString;
                                console.log("QueryStraing", responseObj.countQueryString);
                                responseObj.count = response.result; // adding count to responseobj
                            } else {
                                responseObj.message = "No Records Found";
                            }
                            next(responseObj);
                        }); // end of count code
                        // next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },


    /*
    			 # Method: updateLead
    			 # Description: to update a Lead
    	 */

    updateLead: function(reqBody, token, userLogObject, next) {

        console.log("in update lead RRRRRRRRRRR", reqBody)

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var programUserId = reqBody.providedby;
        var updateInfo = reqBody.updateInfo;
        var leadId = reqBody.leadId;
        var oldRecord={};

        // find criteria for update operation
        var findCriteria = {
            leadId: leadId
        };

        async.series([
            function(callback){
                if(!updateInfo.pointsEarned){
                    Leads.find(findCriteria).exec(function f(err,records){
                        if(err){
                            responseObj.message = err;
                            sails.log.error("Leads-Model>getAllLeads>Leads.find ","Input:"+ findCriteria," error:"+err);
                            callback("error_occured_while fetching lead");
                        }
                        else if (!records){      	// handling in case undefined/no object was returned
                            responseObj.message = "undefined object was returned !!" ;
                            sails.log.error("Leads-Model>getAllLeads>Leads.find ","Input:"+ findCriteria," result:"+records);
                            callback("error_occured_while fetching lead");
                          }
                        else if(records.length===0){		// no record found
                            responseObj.statusCode = 2;
                            responseObj.message = "No record found !!";
                            sails.log.info("Leads-Model>getAllLeads>Leads.find ","Leads records fetched successfully !!");		
                            callback("error_occured_while fetching lead");
                        }else{							// record fetched successfully
                            responseObj.statusCode = 0;
                            responseObj.message = "Leads records fetched successfully !!";
                            oldRecord = records[0];
                            if(oldRecord.pending === false){
                                responseObj.statusCode = -1;
                                if(oldRecord.qualified){
                                    responseObj.message = "Lead is aleady approved";
                                } else {
                                    responseObj.message = "Lead is aleady rejected";
                                }                            
                                callback("error_lead_is_already_approved");
                            } else {
                                callback(null);
                            }		  				
                            
                        }
            
                    });  // end of find query
                } else{
                    callback(null);
                }
               
            },
            function(callback){
                // updating the record
                console.log("updateInfo", updateInfo);
                console.log("findCriteriafindCriteria", findCriteria);
                Leads.update(findCriteria, updateInfo).exec(function(err, records) {
                    console.log("update RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR", err, records);
                    if (err) {
                        responseObj.message = err;
                        sails.log.error("Leads-Model>updateLeads>Leads.update ", "Input:" + updateInfo, " error:" + err);
                        // callback("error in function 3", "function 3 failure");
                    } else if (!records) { // handling in case undefined/no object was returned
                        responseObj.message = "undefined object was returned";
                        sails.log.error("Leads-Model>updateLeads>Leads.update ", "Input:" + updateInfo, " result:" + records);
                        // callback("error in function 3", "function 3 failure");
                    } else if (records.length === 0) { // handling in case records was not updated
                        responseObj.message = "Record not found!!";
                        sails.log.error("Leads-Model>updateLeads>Leads.update ", "Input:" + updateInfo, " result:" + records);
                        // callback("error in function 3", "function 3 failure");
                    } else { // record successfully updated
                        responseObj.message = "Record updated!";
                        responseObj.result = records;
                        responseObj.statusCode = 0;


                        console.log("Records RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRrrrrr", records);
                        var length = responseObj.result.length;

                        for (var a = 0; a < length; a++) {



                            var obj = {
                                    "message": "",
                                    "programId": responseObj.result[a].programId,
                                    "clientId": responseObj.result[a].clientId,
                                    "senderName": responseObj.result[a].senderName,
                                    "senderRoleName": responseObj.result[a].senderRoleName,
                                    "senderId": responseObj.result[a].senderId,
                                    "receiverId": responseObj.result[a].providedby,
                                    "type": "Lead",
                                    "serviceType": "programOperations",
                                    "frontendUserInfo": ""


                                }
                                // if (responseObj.result[a].qualified === true) {
                            if (updateInfo.qualified === true) {
                                console.log("in lead update lead fun qualified true");
                                obj.message = " The status of your lead with lead Id" + responseObj.result[a].leadId + " has been changed to approved by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                                var pointsObj = {
                                    leadId: responseObj.result[a].leadId,
                                    partyId: responseObj.result[a].providedby,
                                    pointsToCredit: 150,
                                    pointsType: "leadQualification",
                                    programId: responseObj.result[a].programId,
                                    clientId: responseObj.result[a].clientId
                                }
                                console.log("before calling cal point");
                                Leads.calculateLeadPoints(pointsObj, userLogObject, token)
                            } else if (updateInfo.rejected === true) {
                                obj.message = " The status of your lead with lead Id" + responseObj.result[a].leadId + " has been changed to rejected by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                            } else if (updateInfo.pending === true) {
                                obj.message = " The status of your lead with lead Id" + responseObj.result[a].leadId + " has been changed to pending by " + responseObj.result[a].senderRoleName + " " + responseObj.result[a].senderName;
                            }

                            console.log("User Notification Object RRRRRRRRRRRRRRRRRRRRRRRRRRRR", obj);
                            console.log("Token RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR", token);

                            if (reqBody.updateInfo.notification) {
                                console.log("in if cond before notification call RRRRRRRRRRRrR");
                                Leads.addUserNotifications(obj, token, userLogObject, function(response) {
                                    console.log("inside addUserNotifications cond before notification call RRRRRRRRRRRrR");

                                    if (response.statusCode === 0) {
                                        responseObj.message = "Lead user notification Added";
                                        responseObj.result = records;
                                        console.log("response object in succes lead RRRRRRRRRRRRRRRR", responseObj);
                                    } else {
                                        console.log("FAILURE");
                                        responseObj.message = "Failed to add user notification of lead";
                                        console.log("response object in failure leads RRRRRRRRRRRRRRRR", responseObj);
                                    }
                                });
                            }
                        }
                    }
                    console.log("response obj final test RRRRRr", responseObj);
                    callback(null);
                });
            }
            ],
            function(err){
                console.log("async series final function",err);
                next(responseObj);
            }
        );
    },


    // To make a service call for lms point calculation.
    calculateLeadPoints: function(reqBody, userLogObject, token, next) {
        console.log("111", reqBody);
        var clientId = reqBody.clientId;
        var programId = reqBody.programId;
        var LeadId = reqBody.LeadId;
        var partyId = reqBody.partyId;
        var pointsToCredit = reqBody.pointsToCredit;
        var pointsType = reqBody.pointsType;
        var transactionId = reqBody.leadId;
        var responseObj = {};
        var parentUsers = [];
        console.log("in calleadpoints RRRRRRRRRRRRRRRRRR", pointsToCredit);

        /********************************   REQUEST OBJECT FOR EXTERNAL API CALLS   ******************************/
        //Request api url and request object for Client User
        var apiUrl1 = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.port +
            ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.url;

        console.log("api frmlead topoint cal RRRRRRRRRRRRR", apiUrl1);

        var requestObject1 = {
            clientId: clientId,
            programId: programId,
            frontendUserInfo: userLogObject,
            programUserIdArray: [partyId],
            pointsToCredit: pointsToCredit,
            pointsType: pointsType,
            transactionId: transactionId
        }

        var requestOptions1 = {
            url: apiUrl1,
            method: ServConfigService.getApplicationAPIs().calculatePointsForLeadManagement.method,
            headers: {
                'authorization': 'Bearer ' +
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDMwNTQyOH0.oVHd01FMVlR56c-4Maiw5TTYNO8yZNND1Iyw2w3hm0DqoAyGNtMXanGuTy_yPZzmVRJUWQgIbYpNVfO_cYOpdw"
            },
            json: requestObject1
        };
        /****************************************************************************************************** */
        console.log("requestOptions1 RRRRRRRRRRRRRRRRRRRRRR", requestOptions1);
        request(requestOptions1, function(error, response, body) {
            console.log("body for calc lead points RRRRRRRRRRRRRRRRRRRRRRRR", body)
            if (error || body === undefined) { // error occured
                responseObj.message = error;
                // callback(error);
            } else {
                if (body.statusCode === 0) { // program found succesfully
                    console.log("in lead point calculation success RRRRRRRRRRRRRRR", body);
                } else if (body.statusCode === 2) {
                    responseObj.statusCode = 2;

                    //SHOULD ADD DEBIT PARTY AS PARENT TO CREDIT PARTY

                } else {
                    responseObj = body;

                }

            }
            // next(responseObj);
        });

    },
    /*
    	# Method: get Hall of Fame for Engineers
    	# Description: to fetch Hall of Fame for Engineers based on number of leads uploaded.
    	*/
    getHallOfFame: function(reqBody, token, next) {
        console.log("request", reqBody);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/
        var startDate = new Date();
        startDate.setMonth(startDate.getMonth() - 1, 1);
        startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
        var endDate = new Date();
        endDate.setDate(endDate.getDate() - (endDate.getDate() - 1))
        var queryString = [
            // Stage 1
            {
                $match: {
                    qualified: true,
                    createdAt: {
                        $gte: startDate,
                        $lte: endDate
                    }
                },
            },
            // Stage 2
            {
                $project: {
                    "projectname": 1,
                    "ownername": 1,
                    "projecttype": 1,
                    "expectedvalue": 1,
                    "address": 1,
                    "city": 1,
                    "state": 1,
                    "pincode": 1,
                    "keyvontacts": 1,
                    "providedby": 1,
                    "providedbyRole": 1,
                    "providedbyName": 1,
                    "createdate": 1,
                    "qualified": 1,
                    "status": 1,
                    "programId": 1,
                    "clientId": 1,
                    "leadId": 1,
                    "createdAt": 1,
                    "updatedAt": 1,
                    "parentUsers": 1
                }
            },

            // Stage 3
            {
                $group: {
                    "_id": {
                        "programUserId": "$providedby"
                    },
                    "data": {
                        $push: "$$ROOT"
                    }
                }
            },

            // Stage 4
            {
                $project: {
                    "_id": 1,
                    "data": 1,
                    "numberOfLeads": {
                        "$size": "$data"
                    }
                }
            },

            // Stage 6
            {
                $lookup: {
                    "from": "ProgramUsers",
                    "localField": "_id.programUserId",
                    "foreignField": "programUserId",
                    "as": "programUserInfo"


                }
            },

            // Stage 7
            {
                $unwind: "$programUserInfo"
            },

            // Stage 8
            {
                $match: {
                    "programUserInfo.programId": reqBody.programId
                }
            },

            // Stage 9
            {
                $project: {
                    "_id": 1,
                    // "data": 1,
                    "numberOfLeads": 1,
                    //"userName": "$programUserInfo.userDetails.userName"
                    "programUserInfo": 1
                }
            },

            // Stage 10
            {
                $group: {
                    "_id": {
                        "numberOfLeads": "$numberOfLeads"
                    },
                    "programUserInfo": {
                        $push: "$programUserInfo"
                    }
                }
            },

            // Stage 11
            {
                $sort: {
                    "_id.numberOfLeads": -1
                }
            },

            // Stage 12
            {
                $limit: 5
            },

        ]




        Leads.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    console.log("records,err", records, err);
                    responseObj.queryString = queryString;
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Hall of Fame fetched successfully";
                        responseObj.result = records;
                        next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },




    /*
    				 # Method: countLeads
    				 # Description: to get count of leads
    */

    countLeads: function(reqBody, token, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var providedby = reqBody.providedby;
        var startDate = reqBody.startDate
        var endDate = reqBody.endDate;
        var countQueryString = [];


        if (reqBody.startDate) {
            var startDate = new Date(startDate).toISOString();
            //startDate.setMonth(startDate.getMonth() - 1);
            //startDate.setDate(startDate.getDate() - (startDate.getDate() - 1))
        }
        if (reqBody.endDate) {
            var endDate = new Date(endDate).toISOString();
            //endDate.setDate(endDate.getDate() - (endDate.getDate() - 1))
        }

        if (reqBody.qualified) {
            countQueryString.push({
                $match : {
                    qualified : reqBody.qualified
                }
            })
        }
        if (reqBody.pending) {
            countQueryString.push({
                $match : {
                    pending : reqBody.pending
                }
            })
        }
        if (reqBody.rejected) {
            countQueryString.push({
                $match : {
                    rejected : reqBody.rejected
                }
            })
        }
        if (reqBody.providedby !== undefined) { //filter using providedby

            countQueryString.push({
                $match: {
                    "$or": [{
                            "providedby": reqBody.providedby
                        },
                        {
                            "parentUsers": {
                                "$in": [reqBody.providedby]
                            }
                        }

                    ],
                    "programId": programId
                },
            });
        }

        if (reqBody.startDate && reqBody.endDate) {
            var startDate = new Date(reqBody.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            var endDate = new Date(reqBody.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            countQueryString.push({
                $match: {
                    "createdAt": {
                        $gte: startDate,
                        $lt: endDate
                    }
                },
            });
        }

        if (reqBody.startDate !== undefined && reqBody.endDate === undefined) { //filter using startDate
            var startDate = new Date(reqBody.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            countQueryString.push({
                $match: {
                    "createdAt": {
                        $gte: new Date(startDate)
                    }
                },
            });
        }

        if (reqBody.endDate !== undefined && reqBody.startDate === undefined) { //filter using endDate
            var endDate = new Date(reqBody.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            countQueryString.push({
                $match: {
                    "createdAt": {
                        $lt: new Date(endDate)
                    }
                },
            });
        }

        countQueryString.push(
            {
                "$project": {
                    "expectedtonnage":1
                ,
                "status1": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                     "$eq": [ "$pending", true ] 
                                },
                                "then": "pending"
                            },
                            {
                                "case": {
                                   "$eq": [ "$qualified", true ] 
                                },
                                "then": "qualified"
                            },
                            {
                                "case": {
                                     "$eq": [ "$rejected", true ] 
                                },
                                "then": "rejected"
                            }
                        ]
                    }
                }
                }
            },
            {
                "$group": {
                    "_id":"$status1",
                    "expectedtonnage": {
                        "$sum": "$expectedtonnage"},
                    "totalCount": {
                        "$sum": 1
                    }
                }
            }
    
        )

        console.log("startDate", startDate);
        console.log("endDate", endDate);
        console.log("ProgramUserId", providedby);
        // NEW CODE ADDED BY TRUPTI TO FETCH COUNT AND TONNAGE
        Leads.native(function(err, collection) {
            console.log("native err",err);
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                console.log("countQueryString",countQueryString);
                collection.aggregate(countQueryString, function(err, records) {
                    console.log("records,err", records, err);
                    responseObj.queryString = countQueryString;
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "count of leads fetched successfully";
                        responseObj.result = records;
                        next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Leads not found";
                        next(responseObj);
                    }
                })
            }
        })

        // OLD CODE TO FETCH COUNT
        // CommonOperations.findCount("Leads", countQueryString, token, function(response) {
        //     responseObj.countQueryString = countQueryString;
        //     // console.log("Response", );
        //     if (response.statusCode === 0) {
        //         responseObj.statusCode = 0;
        //         responseObj.message = "Count Fetched"
        //         responseObj.result = responseObj.count;
        //         responseObj.countQueryString = countQueryString;
        //         responseObj.count = response.result; // adding count to responseobj
        //     } else {
        //         responseObj.message = "No Records Found";
        //     }
        //     next(responseObj);
        // }); // end of count code

    },



    //To Call addUserNotifications
    addUserNotifications: function(reqBody, token, userLogObject, next) {
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        console.log("in add usernot fun addUserNotifications", reqBody);
        var apiURL =
            ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().addUserNotifications.port +
            ServConfigService.getApplicationAPIs().addUserNotifications.url;

        console.log("in add use not fun app url Rrrrrrrrr", apiURL);

        //request options to add User Notifications
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().addUserNotifications.method,
            headers: {
                authorization: "Bearer " + token
            },
            json: reqBody
        };
        console.log("requestOptions in addUserNotifications RRRRRRRRRRRR", requestOptions);
        request(requestOptions, function(error, response, body) {
            console.log("body ..............................", body);
            if (error || body === undefined) {
                responseObj.statusCode = 2;
                responseObj.message = "Error occurred while validating addUserNotifications!!";
            } else {
                if (body.statusCode === 0) {
                    responseObj.statusCode = 0;
                    responseObj.message = "User Notification Added successfully!!";
                    responseObj.result = body.result;
                } else if (body.statusCode === 2) {
                    responseObj.statusCode = 2;
                    responseObj.message = "Failed to add user notifications";
                } else {
                    responseObj = body;
                }
            }
            next(responseObj);
        });
    },

    getLeadReport: function(requ, token, next) {
        console.log("request", requ);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var skip = requ.skip;
        var limit = requ.limit;
        var sort = requ.sort;
        var queryString = [{
            $match: {
                isDeleted: false
            }
        }];
        var countQueryString = {
            isDeleted: false
        };
        // var countQueryString;

        // if (requ.providedby !== undefined) { //filter using providedby
        //     countQueryString.providedby = requ.providedby;
        // }

        // if (requ.leadId !== undefined) { //filter using leadId
        //     countQueryString.leadId = requ.leadId;
        // }

        if (requ.providedby) {

            queryString.push({
                $match: {
                    // isDeleted: false,
                    "$or": [{
                            "providedby": requ.providedby
                        },
                        {
                            "parentUsers": {
                                "$in": [requ.providedby]
                            }
                        }

                    ],
                    "programId": requ.programId
                },
            });
            // countQueryString.isDeleted = false;
            countQueryString.programId = requ.programId;

            countQueryString.$or = [{
                    "providedby": requ.providedby
                },
                {
                    "parentUsers": {
                        "$in": [requ.providedby]
                    }
                }
            ];
            // if (requ.providedby !== undefined) { //filter using providedby
            countQueryString.providedby = requ.providedby;
            //  }
        } else {
            queryString.push({
                $match: {
                    "programId": requ.programId
                },
            });
            countQueryString.programId = requ.programId;
        }

        if (requ.isQualified && requ.isPending) {
            queryString.push({
                $match: {
                    $or: [{
                        "qualified": requ.isQualified
                    }, {
                        "pending": requ.isPending
                    }]
                },
            });

            countQueryString.$or = [{
                    "qualified": requ.isQualified
                },
                {
                    "pending": requ.isPending
                }
            ]
        }

        if (requ.startDate && requ.endDate) {
            var startDate = new Date(requ.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            var endDate = new Date(requ.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            // startDate.toISOString();
            // var endDate = new Date(requ.endDate);
            // endDate.toISOString();
            queryString.push({
                $match: {
                    "createdAt": {
                        // $gte: new Date(requ.startDate),
                        $gte: startDate,
                        $lt: endDate
                    }
                },
            });
            countQueryString.createdAt = {
                $gte: new Date(startDate),
                $lt: new Date(endDate)
            }
        }

        if (requ.startDate !== undefined && requ.endDate === undefined) { //filter using startDate
            var startDate = new Date(requ.startDate);
            startDate.setUTCHours(startDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            startDate.setUTCMinutes(startDate.getUTCMinutes() + 30);
            queryString.push({
                $match: {
                    "createdAt": {
                        $gte: new Date(startDate)
                    }
                },
            });
            countQueryString.createdAt = {
                $gte: new Date(startDate)
            }
        }

        if (requ.endDate !== undefined && requ.startDate === undefined) { //filter using endDate
            var endDate = new Date(requ.endDate);
            endDate.setUTCHours(endDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
            endDate.setUTCMinutes(endDate.getUTCMinutes() + 30);
            endDate.setDate(endDate.getDate() + 1);
            queryString.push({
                $match: {
                    "createdAt": {
                        $lt: new Date(endDate)
                    }
                },
            });
            countQueryString.createdAt = {
                $lt: new Date(endDate)
            }
        }



        if (requ.qualified) {
            queryString.push({
                $match: {
                    "qualified": requ.qualified
                },
            });
            countQueryString.qualified = requ.qualified
        }
        if (requ.rejected) {
            queryString.push({
                $match: {
                    "rejected": requ.rejected
                },
            });
            countQueryString.rejected = requ.rejected;
        }
        if (requ.pending) {
            queryString.push({
                $match: {
                    "pending": requ.pending
                },
            });
            countQueryString.pending = requ.pending;
        }


        if (requ.leadId) {
            queryString.push({
                $match: {
                    "leadId": requ.leadId
                },
            });
            countQueryString.leadId = requ.leadId
        }

        if (requ.status) {
            queryString.push({
                $match: {
                    "status": requ.status
                },
            });
            countQueryString.status = requ.status
        }



        if (sort !== undefined) {
            queryString.push({
                $sort: sort
            });

        }


        if (skip !== undefined) {
            queryString.push({
                $skip: skip
            });

        }
        if (limit !== undefined) {
            queryString.push({
                $limit: limit
            });
        }


        queryString.push({
            $group: {
                "_id": null,
                data: {
                    $push: "$$ROOT"
                }
            }
        }, {
            "$unwind": {
                path: "$data",
                preserveNullAndEmptyArrays: true
            }
        }, );


        // queryString.push({
        //         $lookup: {
        //             "from": "ProgramUsers",
        //             "localField": "data.providedby",
        //             "foreignField": "programUserId",
        //             "as": "programUserInfo"
        //         }
        //     }, {
        //         "$project": {
        //             "userName": "$programUserInfo.userDetails.userName",
        //             "data": 1,
        //             "distributor": { $arrayElemAt: ["$programUserInfo.parentUsers", 0] },
        //         }
        //     }, {
        //         "$unwind": { path: "$distributor", preserveNullAndEmptyArrays: true }
        //     }, {
        //         "$unwind": { path: "$userName", preserveNullAndEmptyArrays: true }
        //     }, {
        //         $lookup: {
        //             "from": "ProgramUsers",
        //             "localField": "distributor.parentUserId",
        //             "foreignField": "programUserId",
        //             "as": "distributorInfo"
        //         }
        //     }, {
        //         "$project": {
        //             "data": 1,
        //             "userName": 1,
        //             "distributorInfo": 1,
        //             "ASM": { $arrayElemAt: ["$distributorInfo.parentUsers", 0] },
        //         }
        //     }, {
        //         "$unwind": { path: "$ASM", preserveNullAndEmptyArrays: true }
        //     }, {
        //         "$lookup": {
        //             "from": "ProgramUsers",
        //             "localField": "ASM.parentUserId",
        //             "foreignField": "programUserId",
        //             "as": "ASMInfo"
        //         }
        //     }, {
        //         "$project": {
        //             "data": 1,
        //             "userName": 1,
        //             "distributorName": "$distributorInfo.userDetails.userName",
        //             "distributorId": "$distributorInfo.programUserId",
        //             "ASMName": "$ASMInfo.userDetails.userName",
        //             "ASMId": "$ASMInfo.programUserId"
        //         }
        //     }, {
        //         "$unwind": { path: "$ASMName", preserveNullAndEmptyArrays: true }
        //     }, {
        //         "$unwind": { path: "$ASMId", preserveNullAndEmptyArrays: true }
        //     }, {
        //         "$unwind": { path: "$distributorName", preserveNullAndEmptyArrays: true }
        //     }, {
        //         "$unwind": { path: "$distributorId", preserveNullAndEmptyArrays: true }
        // });

        queryString.push({
                "$lookup": {
                    "from": "ProgramUsers",
                    "localField": "data.providedby",
                    "foreignField": "programUserId",
                    "as": "programUserInfo"
                }
            },
            // TO DO : filter to get program user which is active and belongs to same program
            {
                "$project": {
                    "data": 1,
                    "distributorId": {
                        "$arrayElemAt": [
                            "$data.parentUsers",
                            0
                        ]
                    },
                    "ASMId": {
                        "$arrayElemAt": [
                            "$data.parentUsers",
                            1
                        ]
                    },
                    programUserInfo: {
                        $filter: {
                            input: "$programUserInfo",
                            as: "programUserInfo",
                            cond: {
                                $and: [{
                                        $eq: ["$$programUserInfo.programId", "PR1518085378956"]
                                    },
                                    {
                                        $eq: ["$$programUserInfo.isDeleted", false]
                                    },
                                ]
                            }
                        }
                    }
                }
            }, {
                "$lookup": {
                    "from": "ProgramUsers",
                    "localField": "distributorId",
                    "foreignField": "programUserId",
                    "as": "distributorInfo"
                }
            }, {
                "$lookup": {
                    "from": "ProgramUsers",
                    "localField": "ASMId",
                    "foreignField": "programUserId",
                    "as": "ASMInfo"
                }
            },
            // TO DO : filter to get program user which is active and belongs to same program
            {
                "$project": {
                    "data": 1,
                    "userName": "$programUserInfo.userDetails.userName",
                    "userMobileNo": "$programUserInfo.userDetails.mobileNumber",
                    "distributorName": "$distributorInfo.userDetails.userName",
                    "ASMName": "$ASMInfo.userDetails.userName",
                    distributorInfo: {
                        $filter: {
                            input: "$distributorInfo",
                            as: "distributorInfo",
                            cond: {
                                $and: [{
                                        $eq: ["$$distributorInfo.programId", "PR1518085378956"]
                                    },
                                    {
                                        $eq: ["$$distributorInfo.isDeleted", false]
                                    },
                                ]
                            }
                        }
                    },
                    asmInfo: {
                        $filter: {
                            input: "$ASMInfo",
                            as: "asmInfo",
                            cond: {
                                $and: [{
                                        $eq: ["$$asmInfo.programId", "PR1518085378956"]
                                    },
                                    {
                                        $eq: ["$$asmInfo.isDeleted", false]
                                    },
                                ]
                            }
                        }
                    }
                }
            },

            {
                "$project": {
                    "data": 1,
                    "userName": 1,
                    "userMobileNo": 1,
                    "distributorName": "$distributorInfo.userDetails.userName",
                    "ASMName": "$asmInfo.userDetails.userName"
                }
            }, {
                "$unwind": {
                    "path": "$userName",
                    "preserveNullAndEmptyArrays": true
                }
            }, {
                "$unwind": {
                    "path": "$userMobileNo",
                    "preserveNullAndEmptyArrays": true
                }
            }, {
                "$unwind": {
                    "path": "$distributorName",
                    "preserveNullAndEmptyArrays": true
                }
            }, {
                "$unwind": {
                    "path": "$ASMName",
                    "preserveNullAndEmptyArrays": true
                }
            }
        );


        Leads.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    responseObj.queryString = queryString;
                    console.log("records,err", records, err);
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "Leads fetched successfully";
                        responseObj.result = records;
                        responseObj.countQueryString = countQueryString;

                        CommonOperations.findCount("Leads", countQueryString, token, function(response) {
                            console.log("Response******************************8", response);
                            if (response.statusCode === 0) {
                                responseObj.statusCode = 0;
                                // responseObj.message = "Count Fetched"
                                // responseObj.result = responseObj.count;
                                responseObj.countQueryString = countQueryString;
                                console.log("QueryStraing", responseObj.countQueryString);
                                responseObj.count = response.result; // adding count to responseobj
                            } else {
                                responseObj.message = "No Records Found";
                            }
                            next(responseObj);
                        }); // end of count code
                        // next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },


    // Added for correction of current records

    updateLeadsforCorrection: function(reqBody, token, next) {

        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var leadsArray = [];
        var clientId = reqBody.clientId;
        var programId = reqBody.programId;
        var userLogObject = {};
        var totalCount=0;
        var successCount=0;
        var errorCount=0;

        Leads.find({programId: programId, clientId:clientId, isDeleted : false}).exec(function(err, records) {
            if (err || !records) {

            } else {
                // 
                for (var i = 0; i < records.length; i++) {
                    var tempObj = {};
                    tempObj.leadId = records[i].leadId;
                    tempObj.providedby = records[i].providedby;
                    tempObj.providedbyRole = records[i].providedbyRole;
                    totalCount++;
                    leadsArray.push(tempObj);
                }

                var count = 0;
                var temp = [];
                var tempLeadsArray = [];
                // console.log("<leadsArray.lengt",leadsArray.length);
                // divide leads array into 400 leads to process at a time to avoid ECONNRESET error
                for(var i=0;i<leadsArray.length;i++){
                    temp.push(leadsArray[i]);
                    count++;
                    if(count%50 === 0){
                        console.log("50 completed",temp.length);
                        tempLeadsArray.push(temp);
                        temp=[];
                    } else if(count ===leadsArray.length){
                        console.log("last",temp.length);
                        tempLeadsArray.push(temp);
                        temp=[];                        
                    }
                }

                console.log("completed",tempLeadsArray.length);
                var countArr = 0;
                var errorLeads = [];
                // LEADS UPDATE 1
                async.eachSeries(tempLeadsArray, function(singleLeadsArray, asynEachSeriesCB) {
                    countArr++;
                    console.log("countArr",countArr);
                    async.each(singleLeadsArray, function(leadObj, asynEachCB) {
                        leadObj.clientId = clientId;
                        leadObj.programId = programId;
                        Leads.getAllParents(leadObj, userLogObject, token, function(response) {
                            // console.log(response);
                            if(response.statusCode===0){
                                successCount++;
                                ("success",leadObj.leadId);
                            }else{
                                errorCount++;
                                errorLeads.push(leadObj);
                                ("error",leadObj.leadId);
                            }
                            asynEachCB(null);
                        });
                    },
                    function(err) {
                        console.log("Update process completed !!",err);
                        if(err){
                            asynEachSeriesCB("Error_occured_while_updating_leads");
                        } else{
                            console.log("error leads",errorLeads.length);
                            asynEachSeriesCB();    
                        }
                    });
                },function(err){
                    console.log("in final async eries function",err);
                    var mailBody = {};
                    mailBody.to = reqBody.userMail;
                    mailBody.isProgramEmail = true;
                    mailBody.from = "info@annectos.net";
                    mailBody.subject = "Lead Correction Update"; 
                    mailBody.text = "Dear admin,<br><br>"; 
                    var errorLeads2=[];                   
                    if(!err){    
                        if(errorLeads.length>0){
                            var errorLeadArray = [];
                            var tempCount=0;
                            var tempArr=[];
                            for(var i=0;i<errorLeads.length;i++){
                                tempArr.push(errorLeads[i]);
                                tempCount++;
                                if(tempCount%50 === 0){
                                    console.log("50 completed",tempArr.length);
                                    errorLeadArray.push(tempArr);
                                    tempArr=[];
                                } else if(tempCount ===errorLeads.length){
                                    console.log("last",tempArr.length);
                                    errorLeadArray.push(tempArr);
                                    tempArr=[];                        
                                }
                            }
                            errorCount = 0;
                            // UPDATE LEADS 2 FOR ERROR LEADS
                            async.eachSeries(errorLeadArray,function(errorLeads,callback){                            
                                async.each(errorLeads, function(leadObj, asyncEachInnerCB){
                                    leadObj.clientId = clientId;
                                    leadObj.programId = programId;
                                    Leads.getAllParents(leadObj, userLogObject, token, function(response) {
                                        // console.log(response);
                                        if(response.statusCode===0){
                                            successCount++;
                                            ("success",leadObj.leadId);
                                        }else{
                                            errorCount++;
                                            errorLeads2.push(leadObj);
                                            ("error",leadObj.leadId);
                                        }
                                        asyncEachInnerCB();
                                    });
                                },function(err){
                                    callback();
                                });
                            },function(err){
                                console.log("error leads after update 1",errorLeads2.length);
                                if(errorLeads2.length>0){
                                    var errorLeadsReqObj = {
                                        errorLeads:errorLeads2,
                                        successCount:successCount
                                    }
                                    // UPDATE LEADS 3 IN CASE OF ERROR LEADS
                                    Leads.updateLeadsInOrder(errorLeadsReqObj,clientId,programId,userLogObject,token,function(response){
                                        successCount = response.successCount;
                                        console.log("error leads after update",response.errorCount,response);
                                        mailBody.text = mailBody.text + "As requested, Lead correction process has been completed on "+(new Date()).toString()+". ";
                                        mailBody.text= mailBody.text + successCount+" leads are updated successfully out of total "+totalCount + " leads.<br><br><br>"; 
                                        console.log("mailBody",mailBody);
                                        mailBody.text= mailBody.text + "Regards."
                                        if(mailBody.to){
                                            CommunicationService.sendEmailWithAttachment(mailBody);
                                        } 
                                    })
                                } else {
                                    console.log("error leads after update",errorCount);
                                    mailBody.text = mailBody.text + "As requested, Lead correction process has been completed on "+(new Date()).toString()+". ";
                                    mailBody.text= mailBody.text + successCount+" leads are updated successfully out of total "+totalCount + " leads.<br><br><br>"; 
                                    console.log("mailBody",mailBody);
                                    mailBody.text= mailBody.text + "Regards."
                                    if(mailBody.to){
                                        CommunicationService.sendEmailWithAttachment(mailBody);
                                    } 
                                }                                
                            })                               
                        }else{
                            mailBody.text = mailBody.text + "As requested, Lead correction process has been completed on "+(new Date()).toString()+". ";
                            mailBody.text= mailBody.text + successCount+" leads are updated successfully out of total "+totalCount + "leads.<br><br><br>";  
                            mailBody.text= mailBody.text + "Regards.";
                            console.log("mailBody",mailBody);
                            if(mailBody.to){
                                CommunicationService.sendEmailWithAttachment(mailBody);
                            } 
                        }                  
                    }else{
                        mailBody.text = mailBody.text + "As requested, Lead correction process has been completed on "+(new Date()).toString()+". ";
                        mailBody.text= mailBody.text + "There was an error while updating the leads. Please try again. <br><br><br>";
                        mailBody.text= mailBody.text + "Regards."
                        console.log("mailBody",mailBody);
                        if(mailBody.to){
                            CommunicationService.sendEmailWithAttachment(mailBody);
                        } 
                    }                                        
                });
            }
        });

    },

    updateLeadsInOrder: function(reqBody,clientId,programId,userLogObject,token,next){
        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        var errorCount=0;
        var errorLeadArray=reqBody.errorLeads;
        var successCount=reqBody.successCount;
        var errorLeadsTemp = [];

        var errorLeadArrayTemp = [];
        var tempCount=0;
        var tempArr=[];
        for(var i=0;i<errorLeadArray.length;i++){
            tempArr.push(errorLeadArray[i]);
            tempCount++;
            if(tempCount%50 === 0){
                console.log("50 completed",tempArr.length);
                errorLeadArrayTemp.push(tempArr);
                tempArr=[];
            } else if(tempCount ===errorLeadArray.length){
                console.log("last",tempArr.length);
                errorLeadArrayTemp.push(tempArr);
                tempArr=[];                        
            }
        }
        console.log("errorLeadArrayTemp",errorLeadArrayTemp.length);
        async.eachSeries(errorLeadArrayTemp,function(errorLeads,callback){                            
            async.each(errorLeads, function(leadObj, asyncEachInnerCB){
                leadObj.clientId = clientId;
                leadObj.programId = programId;
                Leads.getAllParents(leadObj, userLogObject, token, function(response) {                   
                    if(response.statusCode===0){
                        successCount++;
                        ("success",leadObj.leadId);
                    }else{
                        errorCount++;
                        errorLeadsTemp.push(leadObj);
                        ("error",leadObj.leadId);
                    }
                    asyncEachInnerCB();
                });
            },function(err){
                callback();
            });
        },function(err){
            responseObj.successCount=successCount;
            responseObj.errorCount=errorCount;
            responseObj.errorLeads=errorLeadsTemp;
            if(!err){               
                responseObj.statusCode=0;                
            }
            next(responseObj);
        }) 
    },

    sendLeadReportAsAttachmentInMail(reqBody,token, next){
        var report = [];
        var xls;
        var fileName = "_Lead_Report.xlsx";
        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        async.series([
            // Function1: Fetch lead report
            function(callback){
                var reqObj= {
                    programId: "PR1518085378956",
                    clientId: "CL1518008748419"
                }
                Leads.getLeadReport(reqObj, reqBody.token, function(response){
                    if(response.statusCode === 0){
                        console.log("getLeadReport",response.result.length);
                        var result = [];
                        result = response.result;
                        for (var i = 0; i < result.length; i++) {

                            // console.log("AJJJJJJJJJJJJJJJJJJJJJJJJJ-----------,i", i);
                            if (result[i].data.keycontacts) {
                                if (result[i].data.keycontacts.length > 0) {
                                    for (var j = 0; j < result[i].data.keycontacts.length; j++) {
                                        let keyContactsArray = [];
                                        let tempArray = [];
                                        let tempArray1 = [];
                                        var temp = {}
                                        if (j === 0) {
                                            temp["Sr No"] = i + 1;
                                            temp["Project Name"] = result[i].data.projectname;
                                            temp["Owner Name"] = result[i].data.ownername;
                                            temp["Project Type"] = result[i].data.projecttype;
                                            if (result[i].data.expectedtonnage !== undefined
                                                && result[i].data.expectedtonnage !== ""
                                                && result[i].data.expectedtonnage !== null) {
                                                temp["Expected Tonnage"] = result[i].data.expectedtonnage;
                                            } else {
                                                temp["Expected Tonnage"] = 0
                                            }
            
                                            temp["Address"] = result[i].data.address;
                                            temp["City"] = result[i].data.city;
                                            temp["State"] = result[i].data.state;
                                            temp["Pin Code"] = result[i].data.pincode;
                                            // temp.key_contacts = result[i].data.keycontacts;
                                            // temp.provided_by = result[i].data.providedby;
                                            // temp.expectedSalesPeriod = result[i].data.expectedSalesPeriod;
                                            if (result[i].data.expectedSalesPeriod !== undefined
                                                && result[i].data.expectedSalesPeriod !== ""
                                                && result[i].data.expectedSalesPeriod !== null) {
                                                temp["Expected Sales Period"] = result[i].data.expectedSalesPeriod;
                                            } else {
                                                temp["Expected Sales Period"] = "N.A."
                                            }
                                            // temp.provided_by_Role_Name = result[i].data.providedbyRole;
                                            temp["User Name"] = result[i].userName;
                                            // temp.distributor_Id = result[i].distributorId;
                                            temp["Distributor Name"] = result[i].distributorName;
                                            // temp.ASM_Id = result[i].ASMId;
                                            temp["ASM Name"] = result[i].ASMName;
                                            if (result[i].data.pointsEarned !== undefined
                                                && result[i].data.pointsEarned !== ""
                                                && result[i].data.pointsEarned !== null) {
                                                temp["Points Earned"] = result[i].data.pointsEarned;
                                            } else {
                                                temp["Points Earned"] = 0
                                            }
                                            // temp.qualified = result[i].data.qualified;
            
                                            // temp.program_Id = result[i].data.programId;
                                            // temp.client_Id = result[i].data.clientId;
                                            // temp.lead_Id = result[i].data.leadId;
                                            // temp["Lead Added Date"] = (result[i].data.createdAt).toISOString().substring(0, 10); //TODO
                                            var today = new Date(result[i].data.createdAt);
                                            var dd = today.getDate();
                                            var mm = today.getMonth()+1; 
                                            var yyyy = today.getFullYear();
                                            if(dd<10) 
                                            {
                                                dd='0'+dd;
                                            } 
                                            if(mm<10) 
                                            {
                                                mm='0'+mm;
                                            }
                                            temp["Lead Added Date"] = dd+'/'+mm+'/'+yyyy;
                                            // console.log("Lead Added Date",temp["Lead Added Date"]);
                                            // temp.lead_updated_Date = result[i].data.updatedAt.slice(0, 10);
                                            // temp.key_contacts = this.tempArray1;
                                            if (result[i].data.qualified === true) {
                                                temp["Status"] = "Qualified";
                                            } else if (result[i].data.rejected === true) {
                                                temp["Status"] = "Rejected";
                                            } else if (result[i].data.pending === true) {
                                                temp["Status"] = "Pending";
                                            } else {
                                                temp["Status"] = "";
                                            }
                                        } else {
                                            // temp["Sr. No."] = "";
                                            temp["Project Name"] = ""
                                            temp["Owner Name"] = "";
                                            temp["Project Type"] = "";
                                            temp["Expected Tonnage"] = "";
                                            temp["Address"] = "";
                                            temp["City"] = "";
                                            temp["State"] = "";
                                            temp["Pin Code"] = "";
                                            // temp.key_contacts = result[i].data.keycontacts;
                                            // temp.provided_by = "";
                                            temp["Expected Sales Period"] = "",
                                                // temp.provided_by_Role_Name = result[i].data.providedbyRole;
                                                temp["User Name"] = "";
                                            // temp.distributor_Id = "";
                                            temp["Distributor Name"] = "";
                                            // temp.ASM_Id = "";
                                            temp["ASM Name"] = "";
                                            temp["Points Earned"] = "";
                                            // temp.qualified = result[i].data.qualified;
            
                                            // temp.program_Id = "";
                                            // temp.client_Id = "";
                                            // temp.lead_Id = "";
                                            temp["Lead Added Date"] = "";
                                            // temp.lead_updated_Date = "";
                                            // temp.key_contacts = this.tempArray1;
                                            temp["Status"] = "";
                                        }
                                        if (result[i].data.keycontacts[j].keyPersonName) {
                                            temp["Key Contact Name"] = result[i].data.keycontacts[j].keyPersonName;
                                        } else {
                                            temp["Key Contact Name"] = "";
                                        }
            
                                        if (result[i].data.keycontacts[j].keyPersonRole) {
                                            temp["Key Contact Role"] = result[i].data.keycontacts[j].keyPersonRole;
                                        } else {
                                            temp["Key Contact Role"] = "";
                                        }
            
                                        if (result[i].data.keycontacts[j].keyPersonContact) {
                                            temp["Key Contact Mobile No"] = result[i].data.keycontacts[j].keyPersonContact;
                                        } else {
                                            temp["Key Contact Mobile No"] = "";
                                        }
                                        // console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ report11", i);
                                        // console.log("temp",temp["Sr. No."])
                                        report.push(temp);
                                        // console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ report11", i);
                                    }
                                }
                                else {
                                    var temp = {};
                                    temp["Sr No"] = i + 1;                                    
                                    temp["Project Name"] = result[i].data.projectname;
                                    temp["Owner Name"] = result[i].data.ownername;
                                    temp["Project Type"] = result[i].data.projecttype;
                                    // temp.expected_tonnage = result[i].data.expectedtonnage;
                                    if (result[i].data.expectedtonnage !== undefined
                                        && result[i].data.expectedtonnage !== ""
                                        && result[i].data.expectedtonnage !== null) {
                                        temp["Expected Tonnage"] = result[i].data.expectedtonnage;
                                    } else {
                                        temp["Expected Tonnage"] = 0
                                    }
                                    temp["Address"] = result[i].data.address;
                                    temp["City"] = result[i].data.city;
                                    temp["State"] = result[i].data.state;
                                    temp["Pin Code"] = result[i].data.pincode;
                            
                                    // temp.provided_by = result[i].data.providedby;
                                    // temp.expectedSalesPeriod = result[i].data.expectedSalesPeriod;
                                    if (result[i].data.expectedSalesPeriod !== undefined
                                        && result[i].data.expectedSalesPeriod !== ""
                                        && result[i].data.expectedSalesPeriod !== null) {
                                        temp["Expected Sales Period"] = result[i].data.expectedSalesPeriod;
                                    } else {
                                        temp["Expected Sales Period"] = "N.A."
                                    }
                                    // temp.provided_by_Role_Name = result[i].data.providedbyRole;
                                    temp["User Name"] = result[i].userName;
                                    // temp.distributor_Id = result[i].distributorId;
                                    temp["Distributor Name"] = result[i].distributorName;
                                    // temp.ASM_Id = result[i].ASMId;
                                    temp["ASM Name"] = result[i].ASMName;
                                    if (result[i].data.pointsEarned !== undefined
                                        && result[i].data.pointsEarned !== ""
                                        && result[i].data.pointsEarned !== null) {
                                        temp["Points Earned"] = result[i].data.pointsEarned;
                                    } else {
                                        temp["Points Earned"] = 0
                                    }
                                    // temp.qualified = result[i].data.qualified;
                            
                                    // temp.program_Id = result[i].data.programId;
                                    // temp.client_Id = result[i].data.clientId;
                                    // temp.lead_Id = result[i].data.leadId;
                                   // temp["Lead Added Date"] = (result[i].data.createdAt).toISOString().substring(0, 10);
                                    var today = new Date(result[i].data.createdAt);
                                    var dd = today.getDate();
                                    var mm = today.getMonth()+1; 
                                    var yyyy = today.getFullYear();
                                    if(dd<10) 
                                    {
                                        dd='0'+dd;
                                    } 
                                    if(mm<10) 
                                    {
                                        mm='0'+mm;
                                    }
                                    temp["Lead Added Date"] = dd+'/'+mm+'/'+yyyy;
                                    // console.log("Lead Added Date",temp["Lead Added Date"]);
                                    // temp.lead_updated_Date = result[i].data.updatedAt.slice(0, 10);
                                    // temp.key_contacts = this.tempArray1;
                                    if (result[i].data.qualified === true) {
                                        temp["Status"] = "Qualified";
                                    }
                                    if (result[i].data.rejected === true) {
                                        temp["Status"] = "Rejected";
                                    }
                                    if (result[i].data.pending === true) {
                                        temp["Status"] = "Pending";
                                    }
                                    temp["Key Contact Name"] = "";
                                    temp["Key Contact Role"] = "";
                                    temp["Key Contact Mobile No"] = "";
                                    // console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ report22", i);
                                    report.push(temp);
                                }
                            } else {
                                var temp = {};
                                temp["Sr No"] = i + 1;
                                temp["Project Name"] = result[i].data.projectname;
                                temp["Owner Name"] = result[i].data.ownername;
                                temp["Project Type"] = result[i].data.projecttype;
                                // temp.expected_tonnage = result[i].data.expectedtonnage;
                                if (result[i].data.expectedtonnage !== undefined
                                    && result[i].data.expectedtonnage !== ""
                                    && result[i].data.expectedtonnage !== null) {
                                    temp["Expected Tonnage"] = result[i].data.expectedtonnage;
                                } else {
                                    temp["Expected Tonnage"] = 0
                                }
                                temp["Address"] = result[i].data.address;
                                temp["City"] = result[i].data.city;
                                temp["State"] = result[i].data.state;
                                temp["Pin Code"] = result[i].data.pincode;
                        
                                // temp.provided_by = result[i].data.providedby;
                                // temp.expectedSalesPeriod = result[i].data.expectedSalesPeriod;
                                if (result[i].data.expectedSalesPeriod !== undefined
                                    && result[i].data.expectedSalesPeriod !== ""
                                    && result[i].data.expectedSalesPeriod !== null) {
                                    temp["Expected Sales Period"] = result[i].data.expectedSalesPeriod;
                                } else {
                                    temp["Expected Sales Period"] = "N.A."
                                }
                                // temp.provided_by_Role_Name = result[i].data.providedbyRole;
                                temp["User Name"] = result[i].userName;
                                // temp.distributor_Id = result[i].distributorId;
                                temp["Distributor Name"] = result[i].distributorName;
                                // temp.ASM_Id = result[i].ASMId;
                                temp["ASM Name"] = result[i].ASMName;
                                if (result[i].data.pointsEarned !== undefined
                                    && result[i].data.pointsEarned !== ""
                                    && result[i].data.pointsEarned !== null) {
                                    temp["Points Earned"] = result[i].data.pointsEarned;
                                } else {
                                    temp["Points Earned"] = 0
                                }
                                // temp.qualified = result[i].data.qualified;
                        
                                // temp.program_Id = result[i].data.programId;
                                // temp.client_Id = result[i].data.clientId;
                                // temp.lead_Id = result[i].data.leadId;
                               // temp["Lead Added Date"] = (result[i].data.createdAt).toISOString().substring(0, 10); 
                               var today = new Date(result[i].data.createdAt);
                               var dd = today.getDate();
                               var mm = today.getMonth()+1; 
                               var yyyy = today.getFullYear();
                               if(dd<10) 
                               {
                                   dd='0'+dd;
                               } 
                               if(mm<10) 
                               {
                                   mm='0'+mm;
                               }
                               temp["Lead Added Date"] = dd+'/'+mm+'/'+yyyy;
                            //    console.log("Lead Added Date",temp["Lead Added Date"]);
                                // temp.lead_updated_Date = result[i].data.updatedAt.slice(0, 10);
                                // temp.key_contacts = this.tempArray1;
                                if (result[i].data.qualified === true) {
                                    temp["Status"] = "Qualified";
                                }
                                if (result[i].data.rejected === true) {
                                    temp["Status"] = "Rejected";
                                }
                                if (result[i].data.pending === true) {
                                    temp["Status"] = "Pending";
                                }
                                temp["Key Contact Name"] = "";
                                temp["Key Contact Role"] = "";
                                temp["Key Contact Mobile No"] = "";
                                // console.log(" AJJJJJJJJJJJJJJJJJJJJJJJJ report33", i);
                                report.push(temp);
                            }         
                        }
                        callback();
                    }
                })
                },
            // Function2: Convert report in csv format
            function(callback){
                xls = json2xls(report);
                fileName = "_Lead_Report.xlsx";
                fs.writeFileSync(fileName, xls, 'binary');
                callback();
            },
            // Function3: Send email with report in attachment
            function(callback){
                var mailBody = {};
                mailBody.isAttachment = true;
                mailBody.fileName = fileName;
                mailBody.to = ['vidya@annectos.in','varalakshmi@annectos.in','mis@annectos.in','trupti.g@sankeysolutions.com'];
                mailBody.isProgramEmail = true;
                mailBody.from = "info@annectos.net";
                mailBody.subject = "Lead report dated : " + (new Date()).toString();
                CommunicationService.sendEmailWithAttachment(mailBody);
                callback();
            }
        ],
            function(err){
                responseObj.statusCode=0;
                responseObj.message = "Report sent successfully"
                next(responseObj)
            })
    }

}