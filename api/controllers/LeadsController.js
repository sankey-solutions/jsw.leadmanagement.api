/**
 * LeadsController
 *
 * @description :: Server-side logic for managing Leads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var request = require('request');

module.exports = {

  /*
  	# Method: addSecondarySale
  	# Description: to add primary sales in database
  	*/
  addLead: function (req, res) {
    var token = req.token;
    var programInfo = req.programObject;

    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Add Lead";
    userLogObject.eventType = "Add";

    Leads.addLead(req.body, programInfo, token, userLogObject, function (response) {
      res.json(response);
      userLogObject.response = response.message;
      userLogObject.responseData = response;
      LogService.addUserLog(userLogObject, token);
    });
  },

  /*
  	# Method: getLeads
  	# Description: to fetch secondary sales from database based on filter
  	*/
  getLeads: function (req, res) {
    console.log("req.body", req.body);
    var token = req.token;
    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Fetch Leads";
    userLogObject.eventType = "Get";

    Leads.getLeads(req.body, token, function (response) {
      res.json(response);
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },

  /*
   	# Method: updateLead
   	# Description: to update Lead
   	*/
  updateLead: function (req, res) {

    console.log("in update lead",req);
    var token = req.token;
    var programInfo = req.programObject;

    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Update Lead";
    userLogObject.eventType = "Update";

    Leads.updateLead(req.body, token, userLogObject, function (response) {
      res.json(response);
      var resObj = JSON.stringify(response);
      resObj = JSON.parse(resObj);
      userLogObject.responseData = resObj;
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },


  /*
  	# Method: get Hall of Fame for Engineers
  	# Description: to fetch Hall of Fame for Engineers based on number of leads uploaded.
  	*/
  getHallOfFame: function (req, res) {
    console.log("req.body", req.body);
    var token = req.token;
    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Fetch Hall of Fame";
    userLogObject.eventType = "Get";

    Leads.getHallOfFame(req.body, token, function (response) {
      res.json(response);
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },




  /*
  	# Method: countLeads
  	# Description: to count leads from database based on filter
  	*/
  countLeads: function (req, res) {
    console.log("req.body", req.body);
    var token = req.token;
    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Fetch Leads Count";
    userLogObject.eventType = "Get";

    Leads.countLeads(req.body, token, function (response) {
      res.json(response);
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },

  getLeadReport: function (req, res) {
    console.log("req.body", req.body);
    var token = req.token;
    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Fetch Leads Report";
    userLogObject.eventType = "Get";

    Leads.getLeadReport(req.body, token, function (response) {
      res.json(response);
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },



   /*
   	# Method: updateLead
   	# Description: to update Lead
   	*/
     updateLeadsforCorrection: function (req, res) {
      var token = req.token;
      var programInfo = req.programObject;
  
      //Variable defined to create userLogObject
      var userLogObject = req.body.frontendUserInfo;
      userLogObject.requestApi = req.baseUrl + req.path;
      userLogObject.event = "Update Lead";
      userLogObject.eventType = "Update";
      var responseObj = {
        "statusCode": 0,
        "message":"Process for lead correction is initiated. You will recieve an email, once completed.",
        "result": null
      } /*variable defined to send response*/
      res.json(responseObj);
  
      Leads.updateLeadsforCorrection(req.body, token, function (response) {        
        var resObj = JSON.stringify(response);
        resObj = JSON.parse(resObj);
        userLogObject.responseData = resObj;
        userLogObject.response = response.message;
        LogService.addUserLog(userLogObject, token);
      });
    },


    sendLeadReportAsAttachment: function (req, res) {
      console.log("req.body", req.body);
      var token = req.token;
      //Variable defined to create userLogObject
      var userLogObject = req.body.frontendUserInfo;
      userLogObject.requestApi = req.baseUrl + req.path;
      userLogObject.event = "Fetch Leads Report";
      userLogObject.eventType = "Get";
  
      Leads.sendLeadReportAsAttachmentInMail(req.body, token, function (response) {
        res.json(response);
        userLogObject.response = response.message;
        LogService.addUserLog(userLogObject, token);
      });
    },
  

};
