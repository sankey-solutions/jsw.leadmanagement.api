/**
 * CommunicationService.js
 *
 * @description 	 :: Communication Service :  To send sms,email and push notifications to users
 * @created          :: Ajeet
 * @Created  Date    :: 02/04/2016
 * @lastEdited       :: 
 * @lastEdited Date  :: 
 *
 */

"use strict";

var request = require('request');
var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var smtpTransport = require('nodemailer-smtp-transport');

module.exports = {



    /*
    # Method: sendSms
    # Description: to SEND SMS TO USERS
    */

    sendSms: function(reqBody, token) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        //console.log("***reqBody***", reqBody);

        var mobileNumber = reqBody.mobileNumber; // mobile no to which sms to be sent 
        var message = reqBody.message; // message to be sent
        var communicationLogInfo = [];
        var userLength = reqBody.userIds.length;
        for (var i = 0; i < userLength; i++) {
            var logObject = {};

            logObject.communicationType = "SMS";
            logObject.sendTo = mobileNumber;
            logObject.message = message;
            logObject.userId = reqBody.userIds[i];

            communicationLogInfo.push(logObject);
        }


        var reqObject = { // json req object
            "mobileNumber": mobileNumber,
            "message": message,
            "communicationLogInfo": communicationLogInfo
        }



        //console.log("****SMS REQ OBJ****", reqObject);
        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendSms.port +
            ServConfigService.getApplicationAPIs().sendSms.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendSms.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions", requestOptions);

        // consuming the SMS API
        request(requestOptions, function(error, response, body) {
            //console.log("SMS service body is", body);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming SMS API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "SMS sent Successfully!!";
                responseObj.result = mobileNumber;
            } else {
                responseObj = body;
            }
            //next(responseObj);
        });

    },

    //--------------------------------------SEND EMAIL--------------------------------------------------------------------//

    /*
    # Method: sendEmail
    # Description: to sendEmail to users
    */

    sendEmail: function(reqBody, token) {

        //send responseObject
        var responseObj = {
            "statusCode": -1,
            "message": null
        }
        var programId = reqBody.programId;
        var programName = reqBody.programName
        var programUserId = reqBody.programUserId;
        var programUsersIdArray = [];
        programUsersIdArray.push(reqBody.programUserId);
        var clientId = reqBody.clientId;
        //console.log("reqBody", reqBody);
        var communicationLogInfo = [];

        var userLength = reqBody.userIds.length;

        for (var i = 0; i < userLength; i++) {
            var logObject = {};

            logObject.communicationType = "EMAIL";
            logObject.sendTo = reqBody.emailIds[i];
            logObject.message = reqBody.html;
            logObject.userId = reqBody.userIds[i];

            communicationLogInfo.push(logObject);
        }
        var html = [];
        // html.message = "";
        var Unsubscribe = "Unsubscribe";
        for (var a = 0; a < userLength; a++) {
            //console.log("here");
            // html.message = "";
            html[a] = "";
            html[a] = reqBody.html + "<br>" + "<br>" + "<br>" + "<br>" + "You are receiving this email as part of your participation in Loyalty Program" + " " + programName + "." + " " + "If you want to unsubscribe from this mailer, please click <a href=http://localhost:1335/unsubscribe" + "?" + programId + "," + programUserId + "," + clientId + ">" + "" + Unsubscribe
        }
        //reqObject for sending email
        var reqObject = {
            //from: "info@annectos.in",
            to: reqBody.emailIds,
            subject: reqBody.subject,
            // text: reqBody.html,
            text: html,
            // html: reqBody.html + "<a href=http://localhost:1335/unsubscribe>" + "?" + programId + "," + programUserId,
            html: html,
            communicationLogInfo: communicationLogInfo,
            subscribeObject: {
                programId: reqBody.programId,
                programUsersIdArray: programUsersIdArray,
                clientId: reqBody.clientId,
            }
        }

        //console.log("****EMAIL REQ OBJ****", reqObject);

        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.port +
            ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };
        //console.log("reqObject", reqObject);
        //consuming email api
        request(requestOptions, function(error, response, body) {
            //console.log("Email body is", body);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming Email API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Email sent successfully !!";
                responseObj.result = body.result;
            } else {
                responseObj = body;
            }

        });
    },

    /*
    # Method: sendEmail
    # Description: to sendEmail to users
    */

    sendPushNotification: function(reqBody, token) {

        //console.log("notification service called");
        //send responseObject
        var responseObj = {
            "statusCode": -1,
            "message": null
        }

        var notificationDetails = {
            "title": reqBody.title,
            "message": reqBody.message,
            "targetScreen": reqBody.targetScreen
        }

        //reqObject for sending pushNotification
        var reqObject = {
            userIds: reqBody.userId,
            notificationDetails: notificationDetails
        }

        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendPushNotification.port +
            ServConfigService.getApplicationAPIs().sendPushNotification.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendPushNotification.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions", requestOptions);
        //consuming email api
        request(requestOptions, function(error, response, body) {
            //console.log("body is", body, "error is", error);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming notification API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Notification sent successfully !!";
                //responseObj.result = body.result;
            } else {
                responseObj = body;
            }

        });
    },

    //Fetch all transactional event filtered by module name
    getTransactionalEvents: function(reqBody, userLogObject, token, next) {

        var responseObj = {
            "statusCode": -1,
            "message": null,
            "smsInfo": {},
            "emailInfo": {},
            "notificationInfo": {}
        }
        var records;

        var reqObject = {
            moduleName: "Program",
            frontendUserInfo: userLogObject,
            programId: reqBody.programId,
            clientId: reqBody.clientId
        }
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getTransactionalEvent.port +
            ServConfigService.getApplicationAPIs().getTransactionalEvent.url;

        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().getTransactionalEvent.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions", requestOptions);
        request(requestOptions, function(error, response, body) {
            //console.log("body is&&&&", body, "error is", error);
            if (error || body === undefined) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming Transaction API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Events fetched successfully !!";
                responseObj.result = body.result
                records = body.result;

                for (var i = 0; i < records.length; i++) {
                    for (var j = 0; j < records[i].recordInfo.length; j++) {

                        if (records[i].recordInfo[j].subModuleName === "ProgramEnrollment") {
                            if (records[i].communicationType === "SMS") {
                                responseObj.smsInfo.programEnrollment = records[i].recordInfo[j];
                            } else if (records[i].communicationType === "Email") {
                                //console.log("recordINFOOO", records[i].recordInfo[j]);
                                responseObj.emailInfo.programEnrollment = records[i].recordInfo[j];
                            } else {
                                responseObj.notificationInfo.programEnrollment = records[i].recordInfo[j];
                            }
                            break;
                        }
                    }

                    // if (records[i].communicationType === "SMS") {
                    //     for(var j=0; j<records[i].recordInfo; j++){
                    //         if(records[i].recordInfo[j].subModuleName === "ProgramEnrollment"){
                    //             responseObj.smsInfo = records[i].recordInfo;
                    //         }
                    //     }
                    //     //responseObj.smsInfo = records[i].recordInfo;
                    // } else if (records[i].communicationType === "Email") {
                    //     responseObj.emailInfo = records[i].recordInfo;
                    // } else {
                    //     responseObj.notificationInfo = records[i].recordInfo;
                    // }
                }
            } else {
                responseObj = body;
            }
            next(responseObj);
        });
    },


    sendEmailWithAttachment: function(reqBody) {
        //send responseObject
        var responseObj = {
            "statusCode": -1,
            "message": null
        }
        var toEmailId = [];
        var tempTo; //handling incase of single string
        var counter;
        var cc=[], bcc=[];
 

        ////console.log("*****************************EMAIL**********************", reqBody)
        if (reqBody.to !== undefined) {

            if (typeof reqBody.to === "string") {
                tempTo = reqBody.to;
                reqBody.to = [];
                reqBody.to[0] = tempTo;
            }
            if(reqBody.cc)
            cc = reqBody.cc;

            (reqBody.bcc)
            bcc = reqBody.bcc;

            async.eachOf(reqBody.to, function(senderEmailId, index, callback) {
                //PARAMETERS DEFINED TO SEND EMAIL
                var fromEmailId = reqBody.from; //sender Id registered with aws
                var subject = reqBody.subject; //subject for email
                var text = reqBody.text; //text which is to be defined to sending email

                toEmailId = [];
                toEmailId[0] = reqBody.to[index]; //array of recepientd

                //create reusable transporter object using the SES transport
                if (reqBody.isProgramEmail) {
                    var transporter = nodemailer.createTransport(smtpTransport({
                        service: 'SES',
                        auth: {
                            user: ServConfigService.getApplicationConfig().smtpEmailCredentials.user,
                            pass: ServConfigService.getApplicationConfig().smtpEmailCredentials.pass
                        }
                    }));
                    // setup email data with unicode symbols
                    fromEmailId = reqBody.from;
                    ////console.log('FROMMM', fromEmailId);
                    var mailOptions = {
                        //from: fromEmailId,
                        from: reqBody.from,
                        to: toEmailId,
                        subject: subject,
                        cc : cc,
                        bcc: bcc
                        //text: text
                    }
                } 
                else {
                    var transporter = nodemailer.createTransport(ses({
                        accessKeyId: ServConfigService.getApplicationConfig().emailCredentials.accessKeyId,
                        secretAccessKey: ServConfigService.getApplicationConfig().emailCredentials.secretAccessKey
                    }));
                    fromEmailId = ServConfigService.getApplicationConfig().from;
                    ////console.log('FROMMM', fromEmailId);
                    var mailOptions = {
                        //from: fromEmailId,
                        from: ServConfigService.getApplicationConfig().from,
                        to: toEmailId,
                        subject: subject,
                        cc : cc,
                        bcc: bcc
                        //text: text
                    }
                }

                if (reqBody.html !== undefined) {
                    // var newDate = new Date();
                    // newDate.setUTCHours(newDate.getUTCHours() + 5); //converting to IST by adding 5 hours and 30 min
                    // newDate.setUTCMinutes(newDate.getUTCMinutes() + 30);
                    // mailOptions.html = "[" + newDate.toString().slice(0, 21) + "]" + reqBody.html;
                    mailOptions.html = reqBody.html;
                }
                if (reqBody.text !== undefined) {
                    mailOptions.html = text;
                }

                if(reqBody.isAttachment){
                    mailOptions.attachments = [
                        {   // file on disk as an attachment
                            filename: reqBody.fileName,
                            path:  reqBody.fileName// stream this file
                        },
                    ]
                }

                console.log("mailOptions", mailOptions);
                // send mail with defined transport object

                responseObj.successResult = [];
                responseObj.errorResult = [];

                transporter.sendMail(mailOptions, function(error, result) {
                    if (error) {
                        console.log("error", error)
                        responseObj.errorResult.push(error);
                        callback();
                    } else {
                        console.log("result", result);
                        responseObj.from = fromEmailId;
                        responseObj.statusCode = 0;
                        responseObj.successResult.push(result);
                        callback();
                    }
                });
            }, 
            function(err) {
                responseObj.message = "Lead report send to " + responseObj.successResult.length + " emails ";
                console.log("Response of email ids:",responseObj);
                //next(responseObj);
            })// end of asyn each
                            
        }
    },


}