/**
 * ApplicationConfig.js
 *
 * @created          :: Ajeet
 * @Created  Date    :: 10/01/2017
 */

"use strict";

var request = require('request');

//code to allow self signed certificates
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};


module.exports = {


    /**
     *	Method		:: getApplicationConfigDetails
     *	Description :: used to retrieve Application Configurations from Phoenix II DB
     *  Logic       :: Get URL for app config from Client Application config  
     */
    getApplicationConfigDetails: function(setAppConfigCallback, passOnCallbak) {
        var appConfig;
        var clientAppConfig;
        var secretKey;
        var dummyUserId;

        var requestOptions = {
            url: '',
            method: '',
            headers: {
                'authorization': ''
            },
            json: {}
        };

        // Retrieve Application Configuration
        ClientApplicationConfig.find().limit(1).exec(function (err, result) {
            if(result.length){
                clientAppConfig = result[0];
                // console.log("APP config fetched",result[0]);
                // requestOptions.url = "http://localhost:1331/getApplicationConfig/v1";
                requestOptions.url = clientAppConfig.base_url + ":" + clientAppConfig.api_details.getApplicationConfig.port + clientAppConfig.api_details.getApplicationConfig.url;
                requestOptions.method = clientAppConfig.api_details.getApplicationConfig.method;

                secretKey = clientAppConfig.phoenixIISecretToken;
                dummyUserId = "DU132390023";
                var token = JWTService.issue({ id: dummyUserId },clientAppConfig.phoenixIISecretToken);
                requestOptions.headers.authorization = "Bearer " + token;
                // console.log("token-------------------------",token);
                console.log("requestOptions-------------------------",requestOptions);
                
                request(requestOptions, function(error, response, body) {
                    //console.log("error",error ,"response",response,"body",body);
                    if (!error) {
                        if (body.statusCode === 0) {
                            appConfig = body.result[0];
                            setAppConfigCallback(appConfig, passOnCallbak);
                        } else {
                            appConfig = false;
                            sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", body);
                            setAppConfigCallback(appConfig, passOnCallbak);
                        }
                    } else {
                        appConfig = false;
                        sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", body);
                        setAppConfigCallback(appConfig, passOnCallbak);
                    }
                })
            }else if(!err){
                appConfig = false;
                sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", result);
                setAppConfigCallback(appConfig, passOnCallbak);
            }else {
                appConfig = false; 
                sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", err);
                setAppConfigCallback(appConfig, passOnCallbak);
            }
        });

    }
};